require 'rootplace_auth/version'
require 'net/http'

module RootplaceAuth
    def self.included(_base)
        _base.send(:before_action, :is_auth)
    end

    def is_auth
        if ENV['RAILS_ENV'] === 'production' && !session[:share_url]
            if cookies[:rp_token]
                request_api
            else
                redirect_to_rootplace('401')
            end
        end
    end

    def request_api
        project_name = Rails.application.class.parent_name
        uri = URI.parse("https://api.rootplace.com/v1/user/me/#{project_name}")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true

        request = Net::HTTP::Post.new(uri.path, 'Content-Type' => 'application/json')
        request['rpAuthorization'] = cookies[:rp_token]

        response = http.request(request)
        redirect_to_rootplace(response.code)
    end

    def redirect_to_rootplace(code)
        rp_dash_url = 'https://rootplace.com/'
        if code === '401'
            redirect_to "#{rp_dash_url}login?continue=#{request.original_url}"
        elsif code != '200'
            redirect_to rp_dash_url.to_s
        end
    end
end
