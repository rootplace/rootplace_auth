# RootplaceAuth
Gem for authentication with rootplace before loading the staging server of a project, so the staging servers are just public to Rootplace developers and clients on the project

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rootplaceAuth', :git => "git@gitlab.com:rootplace/rootplace_auth.git"
```

And then execute:

    $ bundle

## Usage

To use it you just include it in the application controller like so:

```ruby
# application_controller.rb
require 'rootplace_auth'
class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    include RootplaceAuth
```
